student_data = """A 12 14 16
B 5 6 7
C 17 20 23
D 2 40 12
E 3 41 13
F 7 8 9
G 4 5 6"""

def parse_input(input_str: str):
    lines = input_str.split('\n')
    students = {}
    
    for line in lines:
        parts = line.split()
        name = parts[0]
        marks = list(map(int, parts[1:]))
        students[name] = marks
    return students

def average(students : dict) -> dict:
    averages = {}
    for name, marks in students.items():
        averages[name] = sum(marks) / len(marks)    
    return averages

def sort_students(averages):
    sorted_students = sorted(averages.items(), key=lambda x: x[1])
    return sorted_students

def generate_output(sorted_students):
    output1 = ' < '.join([student[0] for student in sorted_students[:5]])
    output2 = ' < '.join([student[0] for student in sorted_students[5:]])
    
    return f"{output1}\n{output2}"

def ranking(student_data: str):
    return generate_output(sort_students(average(parse_input(student_data))))
    
    
print(ranking(student_data))
